# pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org requests
# import requests
import os
import logging


def dns_making(hostname):
    if "s" in hostname:
        hostname = hostname + ".delta.sbrf.ru"
    return hostname


def ping(hosts, ping_count, ping_time, make_dns):
    broken = []
    for hostname in hosts:
        if hostname:
            if make_dns:
                dns_making(hostname)
            command = f"ping -c {str(ping_count)} -t {str(ping_time)} {hostname}"
            print(command)
            response = os.system(command)
            if response != 0:
                broken.append(hostname)
    hosts = broken
    return hosts


def main():
    # Инициация логгера
    logging.basicConfig(filename="alpha.log", level=logging.INFO)

    # stats = os.system("netstat -a | sed 's/:/ /g' | awk '{print $6}' | sort -u | grep '\.'")
    # with open("hosts.yml") as hosts:
    #     hosts.write(stats)

    with open("venv/hosts.yml", "r") as file:
        hosts = [row.strip() for row in file]

    ping(hosts, 1, 1, make_dns=1)
    # Повторная проверка недоступных хостов - с большими кулдауном и числом попыток
    ping(hosts, 5, 5, make_dns=0)

    for hostname in hosts:
        logging.error(hostname)
    return 0


main()
