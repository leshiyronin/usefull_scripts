import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point

import paramiko
import yaml
import getpass


class Influxdb:
    def __init__(self, config):
        # Захват конфига из файла в формате yml, путь к которому указан в {config}
        if config:
            with open(config, 'r') as f:
                config = yaml.safe_load(f)
        # Настройки подключения к хосту Influxdb по ssh
        if config:
            self.host = config['host']
            self.username = config['username']
        else:
            print('input ssh username')
            self.username = input()
            print('input ssh host')
            self.host = input()
        # Безопасный ввод пароля
        self.secret = getpass.getpass(prompt="input password:")
        self.ssh_client = None

        # Настройки influxdb
        if config:
            self.db_directory = config['db_directory']
        else:
            print("db directory:")
            self.db_directory = input()

        self.influx_url = f'{self.host}:8086'
        self.client = influxdb_client.InfluxDBClient(url=self.influx_url)
        self.write_api = self.client.write_api(write_options=SYNCHRONOUS)
        self.stdin = None
        self.stdout = None
        self.stderr = None
        self.databases = []

    def connect(self):
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh_client.connect(self.host, username=self.username, password=self.secret)
        return 0

    def disconnect(self):
        try:
            self.client.close()
        except AttributeError:
            print("that's AttributeError")
        return 0

    def command_shell(self, command=None):
        self.stdin, self.stdout, self.stderr = self.ssh_client.exec_command(command)
        return 0

    def get_databases(self):
        self.command_shell(command=f'ls /add/influxdb/{self.db_directory}/')
        self.databases = []
        for i in self.stdout.read().decode().split():
            self.databases.append(i)

    def show_databases(self):
        for i in self.databases:
            print(i)
        self.disconnect()

    # def retention(self):
    #     database = 'telegraf'
    #     retention_policy = 'autogen'
    #
    #     bucket = f'{database}/{retention_policy}'
    #
    #     with InfluxDBClient(url='http://localhost:8086', token=f'{self.username}:{self.secret}', org='-') as client:
    #         with client.write_api() as write_api:
    #             print('*** Write Points ***')
    #
    #             point = Point("mem").tag("host", "host1").field("used_percent", 25.43234543)
    #             print(point.to_line_protocol())
    #
    #             write_api.write(bucket=bucket, record=point)
    #
    #         print('*** Query Points ***')
    #
    #         query_api = client.query_api()
    #         query = f'from(bucket: \"{bucket}\") |> range(start: -1h)'
    #         tables = query_api.query(query)
    #         for record in tables[0].records:
    #             print(f'#{record.get_time()} #{record.get_measurement()}: #{record.get_field()} #{record.get_value()}')
