import paramiko
from influxdb import Influxdb


class Connection(Influxdb):
    def __init__(self, config):
        super().__init__(config)
        self.Influxdb = Influxdb
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.stdin = None
        self.stdout = None
        self.stderr = None

    def ssh_connect(self):
        self.client.connect(self.Influxdb.host, username=self.Influxdb.username, password=self.Influxdb.secret)

    def ssh_disconnect(self):
        try:
            self.client.close()
        except AttributeError:
            print("that's AttributeError")

    def ssh_command(self, command=None):
        self.stdin, self.stdout, self.stderr = self.client.exec_command(command)
