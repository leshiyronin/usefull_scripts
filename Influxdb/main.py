from influxdb import Influxdb


def main():
    config = "config.yml"
    influx = Influxdb(config)
    influx.connect()
    influx.get_databases()
    influx.show_databases()
    return 0


main()
